
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, NativeModules, NativeEventEmitter } from 'react-native';
// const navigation = useNavigation();

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const { Counter } = NativeModules;

const counterEvents = new NativeEventEmitter(Counter);

type Props = {};
type State = { count: number }

export default class App extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state     = {
      count: Counter.initialValue,
    };
    this.getCount  = this.getCount.bind(this);
    this.increment = this.increment.bind(this);
    this.decrement = this.decrement.bind(this);
  }

  componentWillMount() {
    counterEvents.addListener('onIncrement', ({ count }) => {
      this.setState({ count });
    });
    counterEvents.addListener('onDecrement', ({ count }) => {
      this.setState({ count });
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome 2 React Native!</Text>
       
        <Button title={'Invoke VTO'} onPress={()=>{
          Counter.increment()
        }} />
      </View>
    );
  }

  getCount() {
    Counter.getCount(count => this.setState({ count }));
  }

  increment() {
    Counter.increment();
  }

  decrement() {
    Counter.decrement()
      .then(message => console.log(message))
      .catch(error => console.error(error));
  }

  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
